package ru.javazen.site.dao;

import ru.javazen.site.model.User;

import java.util.List;

/**
 * Created by ������ on 28.07.2015.
 */
public interface UserDao {
    public List<User> getAllUsers();

    public User findUserById(Long id);

    public User saveUser(User user);
}
