package ru.javazen.site.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.javazen.site.model.User;

import java.util.List;

/**
 * Created by ������ on 28.07.2015.
 */
//TODO
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    //TODO
    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers() {
        return sessionFactory.getCurrentSession().createQuery("FROM USERS").list();
    }

    //TODO
    @Override
    public User findUserById(Long id) {
        List<User> users = getAllUsers();
        for (User user : users) {
            if(user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    //TODO
    @Override
    public User saveUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
        return user;
    }
}
