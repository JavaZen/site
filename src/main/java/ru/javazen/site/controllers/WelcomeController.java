package ru.javazen.site.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Andrew on 19.07.2015.
 */

@Controller
public class WelcomeController {
    private int visitorCount = 0;

    @RequestMapping("/index")
    public String index(Model model) {
        model.addAttribute("visitorCount", visitorCount++);
        return "index";
    }
}
